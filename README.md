# CI/CD components for Python

This project provides [CI/CD components](https://git.ligo.org/help/ci/components/index.html)
to help configure a [GitLab CI/CD](https://git.ligo.org/help/ci/index.html) pipeline
for a Python project.

[[_TOC_]]

## Usage

You can add the individual component templates to an existing `.gitlab-ci.yml`
file by using the `include:` keyword, where `<VERSION>` is the latest released
tag.

See the
[`example/`](https://git.ligo.org/computing/gitlab/components/python/-/tree/main/example/)
project for a demonstration of how this component set may be used in a real
project.

### Components

#### code-quality

Configures a `flake8-code_quality` job that uses `flake8` to analyse the project
for potential issues.

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/python/code-quality@<VERSION>
```

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `python` | `"python"` | Name/path of Python interpreter to use |
| `stage` | `test` | The pipeline stage to add jobs to |
| `project_dir` | `"."` | Directory to scan with flake8 (relative to project root) |

#### dependency-scanning

Configures a `dependency_scanning` job that uses that analyses the dependencies
of the project for known vulnerabilities.

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/python/dependency-scanning@<VERSION>
```

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `test` | The pipeline stage to add jobs to |

Notes:

-   This component builds on GitLab's upstream
    [dependency scanning](https://git.ligo.org/help/user/application_security/dependency_scanning/index.html)
    template to add support for projects that only provide `pyproject.toml` and no
    other requirements or metadata files.

#### test

Configures jobs to test a Python project.

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/python/test@<VERSION>
    inputs:
      extra: test
```

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `job_name` | `sdist` | The name to give the job. |
| `stage` | `test` | Pipeline stage to add jobs to |
| `python` | `"python"` | Name/path of Python interpreter to use |
| `needs` | `[]` | List of jobs whose artifacts are needed by the test jobs |
| `install_target` | `"."` | Path/file/package to install (supports wildcards) |
| `install_extra` | None | Name of the [extra feature group(s)](https://packaging.python.org/en/latest/guides/writing-pyproject-toml/#dependencies-and-requirements) to install (comma-separated) |
| `pytest_options` | `"-ra -v"` | Extra options to pass to pytest |
| `coverage_rcfile` | None | Path to coverage.py configuration file (helps with providing accurate coverage measurements, autodiscovered in most cases) |

Notes:

-   The tests are executed using [pytest](https://pytest.org/) and automatically
    include coverage reporting using [pytest-cov](https://pypi.org/project/pytest-cov/)
    and [Unit test reports](https://git.ligo.org/help/ci/testing/unit_test_reports.html).

-   The default `pytest_options` value `"-ra -v"` enables some extra debugging
    information relating to skipped tests, and prints each test on its own line.

-   An extra `python_coverage` job will be configured in the `.post` pipeline
    stage to combine the coverage reports from individual jobs and post a single
    coverage statistic for the pipeline.

#### publish

Configures a job to publish a Python project.

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/python/publish@<VERSION>
```

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `python` | `"python"` | Name/path of Python interpreter to use |
| `stage` | `deploy` | The pipeline stage to add jobs to |
| `files` | `"*.tar.* *.whl"` | Filename or glob pattern(s) for files to publish" |
| `twine_options` | `"--verbose"` | Options to pass to `twine upload` |

Notes:

-   The `publish` component uses [twine](https://twine.readthedocs.io) to
    interact with PyPI, and requires a token for authentication.
    For details on creating a token, see <https://pypi.org/help/#apitoken>.

    The token should be stored as a
    [group or project variable](https://git.ligo.org/help/ci/variables/index.md#add-a-cicd-variable-to-a-project)
    with the name `TWINE_PASSWORD`, and should ***not*** be stored in the
    project `.gitlab-ci.yml` file.

-   By default, the `publish` component will attempt to publish all tarballs
    (matching `*.tar.*`) and all Python wheels (matching `*.whl`) from _all_
    previous-stage jobs in the pipeline.

    To ensure that only the correct files are published, it is recommended
    that you specify the `files` input with the appropriate patterns, e.g:

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/publish@<VERSION>
        inputs:
          files: "myproject-{*.tar.*,*.whl}"
    ```

-   The `publish` component works with the GitLab project
    [package registry](https://git.ligo.org/help/user/packages/pypi_repository/index.html).
    To publish a package to that registry, use the following value for the
    `twine_options` input:

    ```yaml
    twine_options: "-u gitlab-ci-token -p $CI_JOB_TOKEN --repository-url $CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/pypi"
    ```

#### sdist

Configure a job to build a source distributions for the project.

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/python/sdist@<VERSION>
```

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `job_name` | `sdist` | The name to give the job. |
| `image` | [`"python"`](https://hub.docker.com/_/python) | Container image in which to build the sdist |
| `python` | `"python"` | Name/path of Python interpreter to use |
| `stage` | `build` | The pipeline stage to add jobs to |
| `project_dir` | `"."` | Python project root directory |
| `build_options` | `""` | Extra options to pass to `python-build` |

#### wheel

Configure job to build binary wheels for the project.

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/python/wheel@<VERSION>
```

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `build` | The pipeline stage to add jobs to |
| `project_dir` | `"."` | Python project root directory |
| `pure_python` | `true` | If `true` build a single wheel for all platforms and Python versions, otherwise configure a matrix of jobs to build for different versions |
| `image` | None | Container image in which to build wheels, default is [`python`](https://hub.docker.com/_/python) (if `pure_python: true`) or [`quay.io/pypa/manylinux_2_28_x86_64`](https://quay.io/repository/pypa/manylinux_2_28_x86_64) (`pure_python: false`) |
| `python_versions` | `["3.8", "3.9", "3.10", "3.11", "3.12"]` | The list of Python versions to build when building with `pure_python: false` |
| `job_name` | `"wheel"` | Name to give the job (or matrix of jobs). When building for multiple platforms, give each set a different value for `job_name`. |
| `runner_tags` | `[]` | List of runner [`tags`](https://git.ligo.org/help/ci/yaml/#tags) to apply to the job. |
| `build_options` | `""` | Extra options to pass to `python -m build` |
| `delocate_options` | `""` | Extra options to pass to the wheel delocation tool (auditwheel on linux, delocate on macOS) |

Notes:

-   All wheels are built using the [`python-build`](https://build.pypa.io/) frontend.

-   Projects with any compiled components should include the `python/wheel`
    component once **per target platform**, specifying `pure_python: false` and
    a different `job_name` and `runner_tags` list for each platform, e.g:

    ```yaml
    include:
      # linux wheels
      - component: git.ligo.org/computing/gitlab/components/python/wheel@<VERSION>
        inputs:
          pure_python: false
          job_name: wheel_linux_x86_64
      # macos wheels
      - component: git.ligo.org/computing/gitlab/components/python/wheel@<VERSION>
        inputs:
          pure_python: false
          runner_tags: [macos]
          job_name: wheel_macos_x86_64
      - component: git.ligo.org/computing/gitlab/components/python/wheel@<VERSION>
        inputs:
          pure_python: false
          runner_tags: [macos_arm64]
          job_name: wheel_macos_arm64
    ```

    This will configure a pipeline with one wheel build job **per platform**
    **per python version**, each uploading a single wheel file as an artifact.
    The `runner_tags` are used to ensure that the jobs land on a runner machine
    with the right platform and architecture.

    For details of the available runner tags, see

    <https://computing.docs.ligo.org/guide/gitlab-ci/#runners>

### Meta-components

The following are components that just bundle together logical groups of
other components, to give you an set of jobs that enrich your CI/CD pipeline.

#### qa

Configures jobs to perform quality assurance (Q/A) scans of your Python project.

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/python/qa@<VERSION>
```

This meta-component combines the following individual components

-   [`computing/gitlab/components/python/code-quality`](#code-quality)
-   [`computing/gitlab/components/python/dependency-scanning`](#dependency-scanning)
-   [`computing/gitlab/components/sast`](https://git.ligo.org/explore/catalog/computing/gitlab/components/sast)

##### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `test` | Pipeline stage to add jobs to |
| `python` | `"python"` | Name of the Python interpreter to call |

## Contributing

Please read about CI/CD components and best practices at: <https://git.ligo.org/help/ci/components/index.html>.

All interactions related to this project should follow the
[LIGO-Virgo-KAGRA Code of Conduct](https://dcc.ligo.org/LIGO-M1900037).

For more details on contributing to this project, see `CONTRIBUTING.md`.

## Releases

To create a new release of this component, go to
<https://git.ligo.org/computing/gitlab/components/python/-/tags/new>
and create a new tag.
**You must use a semantic version for the tag name**, e.g. 1.2.3, without
any prefix or suffix.
Feel free to include a tag message, but this is not required.

The CI/CD pipeline triggered for the tag will then automatically create a new
[release](https://git.ligo.org/computing/gitlab/components/python/-/releases)
which will be published to the
[CI/CD Catalog](https://git.ligo.org/explore/catalog/computing/gitlab/components/python).
