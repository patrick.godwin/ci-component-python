# `python/code-quality`

Configure a job to scan a Python project for issues using
[Flake8](https://flake8.pycqa.org).

## Description

This component configures a single job called `flake8-code_quality`
that installs `flake8`, and then runs the tool to produce a GitLab
[Code Quality report](https://git.ligo.org/help/ci/testing/code_quality.html).

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/python/code-quality@<VERSION>
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `project_dir` | `"."` | Python project path to scan |
| `python` | `"python"` | Name of the Python interpreter to call |
| `requirements` | None | Extra requirements to install with `pip`. |
| `stage` | `"test"` | Pipeline stage to add job to. |

## Customisation

All flake8 customisation should be handled via the supported configuration
files; probably one of `pyproject.toml` or `.flake8`.

For details, see <https://flake8.pycqa.org/en/stable/user/configuration.html>.

## Examples

!!! example "Scan a Python project"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/code-quality@<VERSION>
    ```
