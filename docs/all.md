# `python/all`

Configure an end-to-end Python workflow for this project.

## Description

This component automatically configures a workflow to

- [scan the project](./qa.md) for security, and code-quality issues
- build a [source distribution](./sdist.md)
- build a [binary distribution](./wheel.md) (pure-Python projects only)
- [test](./test.md) the project
- [publish](./publish.md) the distributions to [PyPI](https://pypi.org)

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/python/all@<VERSION>
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `project_dir` | `"."` | Python project path (containing the `pyproject.toml` file) |
| `python` | `"python"` | Name of the Python interpreter to call |
| `python_versions` | `["3.8", "3.9", "3.10", "3.11", "3.12"]` | The list of Python versions to build when building with `pure_python: false` |
| `sdist` | `true` | Build a source distribution (tarball) |
| `wheel` | `true` | Build a binary distribution (wheel) |
| `install_extra` | None | Name(s) of the [extra feature group(s)](https://packaging.python.org/en/latest/guides/writing-pyproject-toml/#dependencies-and-requirements) to install (comma-separated) when testing |
| `extra_test_commands` {: .nowrap } | `[]` | Extra test commands to run during the test job(s) (after running pytest) |

## Notes

### Wheels for compiled projects {: #wheels }

The `python/all` meta-component isn't flexible enough to configure building
wheels for Python projects that compile extension modules.

See the dedicated [`python/wheel`](./wheel.md) component for instructions
on configuring a pipeline to build wheels for compiled projects.

## Customisation

Almost all of the scanning, build, test, and publish behaviour can be
configured via the
[`pyproject.toml`](https://packaging.python.org/en/latest/guides/writing-pyproject-toml/)
project configuration file.

See the _Customisation_ sections for the relevant sub-components for
any other relevant details.

## Examples

!!! example "Build and test a Python application"

    To configure a pipeline that builds for the latest versions of Python,
    and installs the `[test]` optional-dependencies group for testing:

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/all@<VERSION>
        inputs:
          install_extra: "test"
          python_versions:
            - 3.10
            - 3.11
            - 3.12
    ```
