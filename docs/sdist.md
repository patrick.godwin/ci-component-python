# `python/sdist`

Configure a job to build a source distribution for a Python project.

## Description

This component creates a job that uses [`build`](https://build.pypa.io/)
to create a source distribution (tarball) for a Python project.

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/python/sdist@<VERSION>
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `job_name` | `sdist` | The name to give the job. |
| `image` | [`python`](https://hub.docker.com/_/python) | Container image in which to build the sdist |
| `python` | `python` {: .nowrap } | Name/path of Python interpreter to use |
| `stage` | `build` | The pipeline stage to add jobs to |
| `project_dir` | `"."` | Python project root directory |
| `build_options` {: .nowrap } | `""` | Extra options to pass to `python-build` |

## Customisation

The behaviour of the `build` tool should be customised via the
[`pyproject.toml`](https://packaging.python.org/en/latest/guides/writing-pyproject-toml/)
project configuration file.

## Examples

### Building a source distribution {: #build }

!!! example "Build a Python source distribution"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/sdist@<VERSION>
    ```

### Building multiple source distributions for a monrepo {: #monorepo }

For a GitLab project with multiple Python projects included as part of a
monorepo, you can `include` the `python/sdist` multiple times with different
`inputs` to build each subproject:

!!! example "Build distributions for multiple subpackages in a monorepo"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/sdist@<VERSION>
        inputs:
          job_name: sdist_package1
          project_dir: package1/
      - component: git.ligo.org/computing/gitlab/components/python/sdist@<VERSION>
        inputs:
          job_name: sdist_package2
          project_dir: package2/
      - component: git.ligo.org/computing/gitlab/components/python/sdist@<VERSION>
        inputs:
          job_name: sdist_package3
          project_dir: package3/
    ```
