# `python/dependency-scanning`

Configures a `dependency_scanning` job that uses that analyses the dependencies
of the project for known vulnerabilities.

## Description

This component builds on GitLab's upstream
[dependency scanning](https://git.ligo.org/help/user/application_security/dependency_scanning/index.html)
template to add support for projects that only provide `pyproject.toml` and no
other requirements or metadata files.

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/python/all@<VERSION>
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `"test"` | Pipeline stage to add job to. |

## Examples

!!! example "Scan a Python application"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/dependency-scanning@<VERSION>
    ```
