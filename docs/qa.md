# `python/qa`

Configures all jobs to perform quality assurance (Q/A) scans of your Python project.

## Description

This meta-component combines the following individual components

- [`python/code-quality`](./code-quality.md)
- [`python/dependency-scanning`](./dependency-scanning.md)
- [`sast/sast`](https://git.ligo.org/explore/catalog/computing/gitlab/components/sast)

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/python/qa@<VERSION>
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `test` | Pipeline stage to add jobs to |
| `python` {: .nowrap } | `"python"` {: .nowrap } | Name of the Python interpreter to use for [`python/code-quality`](./code-quality.md) |
| `requirements` | None | Extra requirements to install with `pip` for [`python/code-quality`](./code-quality.md). |

## Customisation

See the _Customisation_ sections for the relevant sub-components for
any other relevant details.

## Examples

!!! example "Configure complete quality-assurance scanning for a Python project"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/qa@<VERSION>
    ```
